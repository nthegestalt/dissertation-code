%% File: CubicSplineFilter
%
%  Description: Performs Cubic Spline Interpolation filtering on a signal
%
%  Author: Tuan
%  Last modified: 23rd September 2015
%
function [baseline,conditioned_signal,ti] = CubicSplineFilter(signal,valleys) 
tic;
    spline_est = pchip(valleys(:,1),valleys(:,2));
    baseline = fnval((1:length(signal)),spline_est);
    
    baseline = flip(rot90(baseline));
    conditioned_signal = signal - baseline;
ti = toc;
end
