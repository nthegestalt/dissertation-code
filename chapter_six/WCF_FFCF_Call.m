%% File: WCF_FFCF_Call
%
%  Description: Calls the WCF and FFCF functions and compares their results
%
%  Author: Tuan
%  Last modified: 5th February 2016
%

function WCF_FFCF_Call
    % Parameters
    file_name = '0028_8min';
    sample_file = [file_name '.mat'];
    load(sample_file);
    start = 1;
    sample_length = 18000;

    % Load and prepare the signal
    ppg_signal = (signal.pleth.y(start:sample_length));
    ppg_signal = tsmovavg(ppg_signal, 's', 30,1);
    ppg_signal(1:30) = ppg_signal(30);

    % FFCF - References files from chapters four and five
    [fir_csg,fir_bs,fir_ti] = FIRFFTFilter(ppg_signal);
    [ads_peaks,ads_valleys,line_segments,ads_det_ti] = ADSDetect(fir_csg);
    [fir_cbs_bs,fir_cbs_csg,fir_cbs_ti] = CubicSplineFilter(fir_csg,ads_valleys); 

    % WCF - References files from chapters four and five
    [wv_csg,wv_bs,wv_ti] = WaveletCascadedFilter(ppg_signal',8);
    [derivative,deriv_peaks,wv_valleys, wv_peaks,wv_det_ti]= WaveletCascadedDetect(wv_csg);
    [wv_cbs_bs,wv_cbs_csg,wv_cbs_ti] = CubicSplineFilter(wv_csg,wv_valleys);

    DisplayCBS(ppg_signal,fir_cbs_bs+ fir_bs,ppg_signal - (fir_cbs_bs + fir_bs),'FFCF');
    DisplayCBS(ppg_signal,wv_bs + wv_cbs_bs,ppg_signal -(wv_bs + wv_cbs_bs),'WCF');

    GenerateCorrelationReport(wv_bs,wv_csg,fir_bs,fir_csg,wv_ti,fir_ti,'WCF','FFCF');
end