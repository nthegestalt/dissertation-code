%% File: WaveletCascadedFilter
%
%  Description: Performs a wavelet + cubic spline interpolation filtering
%               on PPG signals to remove the baseline drift. Implementation is based on:
%               -------------------------------------------------------------------------------------------------------------
%               Xu, Lisheng, Zhang, David, Wang, Kuanquan, Li, Naimin, & Wang, Xiaoyun. (2007). 
%               Baseline wander correction in pulse waveforms using wavelet-based cascaded adaptive filter. 
%               Computers in Biology and Medicine, 37(5), 716-731. '
%               doi: http://dx.doi.org/10.1016/j.compbiomed.2006.06.014
%               -------------------------------------------------------------------------------------------------------------
%
%  Author: Tuan
%  Last modified: 5th December 2015
%
%  Param: Signal
%  Return: signal, approximation, runtime
function [csg, approx,ti] =  WaveletCascadedFilter(ppg_signal,scale)
tic; % Start Timer
%     scale = 8;
    wavelet = 'dmey';
    p = nextpow2(length(ppg_signal));
    pad_length = 2^p - length(ppg_signal)/2;
     
     % Pad the signal to avoid boundary errors during transform
    ppg_signal = wextend(1,'zpd',ppg_signal,pad_length);
    padded_sig_length = length(ppg_signal);
    
    % Wavelet filtering
    dwtmode('zpd')
    [C,L] = wavedec(ppg_signal,scale,wavelet);
    
    % Get approximations and details
    approximations = zeros(padded_sig_length, scale);
    details = zeros(padded_sig_length, scale);
    
    % Get only the approximations at 1 and the scale since those two are
    % the only ones we really need to calculate the ER and to approximate
    % the baseline drift/trend within the signal
    approximations(:,1) = wrcoef('a', C, L, wavelet, 1);
    approximations(:,2) = wrcoef('a', C, L, wavelet, scale);
    
    % The baseline is usually the last scale of the approximations
    approx = approximations(:,2);
    
    % Remove the padding from the baseline and the raw signal
    ppg_signal = ppg_signal(pad_length+1:end-pad_length);
    ppg_signal = flip(rot90(ppg_signal));
    approx = approx(pad_length+1:end-pad_length);

    % Calculate the ER
    k1 = (approximations(:,1) - approximations(:,2));
    k2 = approximations(:,2) - mean(approximations(:,2));
    ER = 20 * log(norm(k1 - mean(k1)) / norm(k2));
    
    % If the ER is greater than the threshold then there's no need to
    % proceed with cubic spline filtering
    ER_threshold = 50;
 
    if(ER > ER_threshold)
        csg = ppg_signal;
    else
        csg = ppg_signal - approx;
    end
ti = toc;
end
