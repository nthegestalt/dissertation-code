%% File: CalculateRMSE
%
%  Description: Calculates the RMSE between two signals and their
%               drift approximatioms
%
%  Author: Tuan
%  Last modified: 8th December 2015
%
%  Param: signal one, signal two, approximation one, approximation two
%  Return: signal RMSE, approximation RMSE
function [csg_rmse,apprx_rmse] = CalculateRMSE(csg1,csg2,apprx1,apprx2)
    csg_rmse = rms(csg1-csg2);
    apprx_rmse = rms(apprx1-apprx2);
end