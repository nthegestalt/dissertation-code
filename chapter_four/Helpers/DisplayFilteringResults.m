%% File: CalculateRMSE
%
%  Description: Displays Filtering results 
%
%  Author: Tuan
%  Last modified: 8th December 2015
%
%  Param: signal, baseline, filtered signal, method name
%
function DisplayFilteringResults( sig,bs,csg,method )
    %Chart Parameters
    width = 8;     % Width in inches
    height = 4.8;  % Height in inches
    alw = 0.75;    % AxesLineWidth
    fsz = 14;      % Fontsize
    lw = 1.0;      % LineWidth
    msz = 10;      % MarkerSize
    fnt = 'CMU Sans Serif Demi Condensed';
    
    figure;
    clf;
    set(gcf,'renderer','opengl');
    set(gcf, 'Position', [0 0 width*100, height*100]); %<- Set size
    
    subplot(2,1,1);
    set(gca, 'FontSize', fsz, 'LineWidth', alw,'FontName',fnt, 'FontWeight','bold'); %<- Set properties
    box on;
    hold on;
    plot(sig,'LineWidth',lw,'Color','b');
    plot(bs,'LineWidth',lw,'Color',[1,0,0]);
    title([method ' Drift Approximation']);
    
    subplot(2,1,2);
    set(gcf, 'Position', [0 0 width*100, height*100]); %<- Set size
    set(gca, 'FontSize', fsz, 'LineWidth', alw,'FontName',fnt, 'FontWeight','bold'); %<- Set properties
    box on;
    hold on;
    plot(csg,'LineWidth',lw,'Color','b');
    title('Filtering Result');
end

