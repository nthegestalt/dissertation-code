%% File: CalculateRMSE
%
%  Description: Calculates the correlation between two signals and their
%               drift approximatioms
%
%  Author: Tuan
%  Last modified: 8th December 2015
%
%  Param: signal one, signal two, approximation one, approximation two
%  Return: signal correlation percent, approximation correlation percent
function [csg_corr,apprx_corr] = CalculateCorrelation(csg1,csg2,apprx1,apprx2)
    csg_corr = corrcoef(csg1,csg2);
    csg_corr = csg_corr(1,2);
    apprx_corr = corrcoef(apprx1,apprx2);
    apprx_corr = apprx_corr(1,2);
end