%% File: FIROverlapSave
%
%  Description: Implements the overlap-save algorithm using the FIR filter
%
%  Author: Tuan
%  Last modified: 18th December 2015
%
%  Param: Signal, segment size (To complete segment with filter order)
%  Return: signal, approximation, average runtime, total run time
function [conditioned_signal,approximation,mean_time,sum_time] = FIROverlapSave(x,L)
    % Filter coefficients
    fs = 300;
    fc = 0.2/(fs/2);
    order = 650;
    h = fir1(order,fc,'low',kaiser(order+1,5.5));

   % Make sure these are the same direction
    h = h(:)';
    x = x(:)';
    
    M = length(h);
    M1 = M - 1;
    N = L + M1;
    
    % Pad both ways to prevent border errors due to insufficient samples to
    % form the blocks needed. We can always remove it later on if desired.
    x_pd = [zeros(1,M1) x zeros(1,M1)];
    hzpd = [h zeros(1, N-M1-1)];
    H = fft(hzpd,N);
    xL = length(x_pd);  
    
    start_index = 1;
    end_index = N;
    y = [];
    
    % DEBUG variables
    run_times = [];
    counter = 1;
    
    while(true)
        if(start_index > xL || end_index > xL || length(y) > xL)
           break; 
        end
        
tic;
        x_block = x_pd(start_index:end_index);
        x_fft = fft([x_block zeros(1,N)],N);
        
        yt = x_fft .* H;
        yt = ifft(yt,N);
        
        y = [y yt(M1+1:end)];
        
        % Shift by M1+1 to ensure overlapping
        start_index = (end_index - M1)+1;
        end_index = end_index + L;
run_times(end+1) = toc; % Store run times
        
        counter = counter + 1;
    end
    
    % Remember to shift it by length(h)/2 for time delay
    conditioned_signal = (x - y((order/2):length(x)+(order/2)-1))';
    approximation = (y((order/2):length(x)+(order/2)-1))';
    mean_time = mean(run_times);
    sum_time = sum(run_times);
    assignin('base','run_times',run_times);
    assignin('base','sum_times',sum_time);
    assignin('base','mean_time',mean_time);
end