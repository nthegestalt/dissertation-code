%% File: FIRFFTFilter
%
%  Description: Implements a FIR filter with FFT convolution
%
%  Author: Tuan
%  Last modified: 18th December 2015
%
%  Param: Signal
%  Return: signal, approximation, runtime
function [conditioned_signal, approximation,ti] = FIRFFTFilter(signal)
    % Filter Coefficients
    fs = 300;
    fc = 0.2/(fs/2);
    order = 650;
    
    % Transform the filter coefficients
    b = fir1(order,fc,'low',kaiser(order+1,5.5));
    sig_length = length(signal);
    Nfft = 2^(ceil(log2(sig_length+order-1)));
    b_fft = fft([b zeros(1,Nfft-order-1)]);
    
    % Filtering process
tic;
    ppg_fft = fft([signal' zeros(1,Nfft-sig_length)]);
    trend = ppg_fft.*b_fft;

    trend = ifft(trend);
    trend = real(trend);
    
    approximation = (trend(301:sig_length+300))';
    conditioned_signal = signal - approximation;
ti = toc;
end