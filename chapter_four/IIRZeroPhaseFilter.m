%% File: IIRZerophaseFilter
%
%  Description: Implements an IIR Zerophase filter using a 5th order
%               butterworth filter
%
%  Author: Tuan
%  Last modified: 18th December 2015
%
%  Param: Signal
%  Return: signal, approximation, runtime
function [zeropassed_signal,trend,run_time] = IIRZeroPhaseFilter(signal,cutoff)

    % Extend the signal to prevent border errors
    signal = wextend(1,'zpd',signal,300);
    
    fn=300/2; 
    fc= cutoff / fn;
    order = 6; 
    [b,a]=butter(order,fc,'low');
    
tic;
    trend = filtfilt(b,a,signal);
    trend = trend(301:end-300);
    signal = signal(301:end-300);
    zeropassed_signal =  signal - trend;
run_time = toc;
end