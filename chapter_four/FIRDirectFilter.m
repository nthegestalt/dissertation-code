%% File: FIRDirectFilter
%
%  Description: Implements a FIR filter with direct convolution
%
%  Author: Tuan
%  Last modified: 18th December 2015
%
%  Param: Signal
%  Return: signal, approximation, runtime
function [conditioned_signal,approximation,ti] = FIRDirectFilter(signal)
    % Filter Coefficients
    fs = 300;
    fc = 0.2/(fs/2);
    order = 650;
    h = fir1(order,fc,'low',kaiser(order+1,5.5));
    
    tic; % Only interested in the filtering time, not the setup time
    zpd_signal = [signal; zeros(order/2,1)];
    approximation = filter(h,1,zpd_signal);
    approximation = approximation(order/2+1:end);
    conditioned_signal = signal - approximation;
    ti = toc;
end