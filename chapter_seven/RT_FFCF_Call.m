%% File: RT_FFCF_Call
%
%  Description: Calls the WCF and FFCF functions and compares their results
%
%  Author: Tuan
%  Last modified: 5th February 2016
%

function WCF_FFCF_Call
    % Parameters
    file_name = '0009_8min';
    sample_file = [file_name '.mat'];
    load(sample_file);
    start = 1;
    sample_length = 6000;

    % Load and prepare the signal
    ppg_signal = (signal.pleth.y(start:sample_length));
    ppg_signal = tsmovavg(ppg_signal, 's', 30,1);
    ppg_signal(1:30) = ppg_signal(30);

    % RT-FFCF - References files from chapters four and five
    % Filter Coefficients
    fs = 300;
    fc = 0.2/(fs/2);
    order = 650;
    M = order + 1;
    h = fir1(order,fc,'low',kaiser(order+1,5.5));

    % Run Algorithm
    [facbs_csg_soften,facbs_bs,facbs_peaks,...
     facbs_valleys,facbs_csb_final,facbs_csg_final...
     facbs_mapped_peaks,facbs_mapped_valleys] = FFCFAlgorithm(ppg_signal,h,150,false);

    % WCF - References files from chapters four and five
    [wv_csg,wv_bs,wv_ti] = WaveletCascadedFilter(ppg_signal',8);
    [derivative,deriv_peaks,wv_valleys, wv_peaks,wv_det_ti]= WaveletCascadedDetect(wv_csg);
    [wv_cbs_bs,wv_cbs_csg,wv_cbs_ti] = CubicSplineFilter(wv_csg,wv_valleys);

    DisplayCBS(ppg_signal,facbs_csb_final,ppg_signal - facbs_csb_final','RT-FFCF');
    DisplayCBS(ppg_signal,wv_bs + wv_cbs_bs,ppg_signal -(wv_bs + wv_cbs_bs),'WCF');

    GenerateCorrelationReport(wv_bs+wv_cbs_bs,wv_cbs_csg,facbs_csb_final',facbs_csg_final',wv_ti,NaN,'WCF','RT-FFCF');
end