%% File: FIRADSCBSCascadedFilter
%
%  Description: Implementation of the FIR-ADS-CBS Cascaded Algorithm Demo.
%               Signal is processed using overlap-save and detection is
%               applied continuously. Results of detection and signal are
%               then used in Cubic Spline Interpolation to remove the
%               remaining drift softening couldn't.
%
%  Parameters: x = signal, h =filter coefficients, L = length of chunk,
%              live = toggle drawing while filtering
%
%  Return: Softened signal, Soften baseline approximation, Peaks, Valleys, 
%          Combined baseline, Cubic Filtered Signal
%
%  Author: Tuan
%  Last modified: 19th December 2015k
%

function [soften_line,soften_trend_line,peaks,...
          valleys,combined_trend,cubic_filt_line...
          mapped_peaks, mapped_valleys] = FFCFAlgorithm(x, h, L,live)
    
    % For mapping the features to the conditioned signal
    mapped_peaks = [];
    mapped_valleys = [];

    % Detection test
    detector = CADSRunning;
    
    % Make sure these are the same direction
    h = h(:)';
    x = x(:)';
    
    % Length of segment
    M = length(h);
    M1 = M - 1;
    N = L + M1;
    
    % Pad both ways to prevent border errors due to insufficient samples to
    % form the blocks needed. We can always remove it later on if desired.
    original_xL = length(x);                    % Length of original x
    x = [zeros(1,M1) x zeros(1,M1)];            % Pad the signal
    hzpd = [h zeros(1, N-M1-1)];                % Pad the filter kernel
    H = fft(hzpd,N);                            % Filter FFT
    xL = length(x);                             % Length of padded x
    
    start_index = 1;
    end_index = N;
    
    % Run-times for different parts of the algorithm
    filter_run_times = [];
    cbs_interpolate_times = [];
    
    % For the display as well as the main algorithm
    segment_line = [];
    soften_trend_line = [];
    soften_line = [];
    cubic_filt_line = [];
    cubic_est_line = [];
    first_flag = true;
    pre_valleys = [];
    start_point = NaN;
    
    signal_chunks = {};
    trend_chunks = {};
    combined_trend = [];
    
    counter = 1;
    wait_period = floor((M1/2)/L);  % Calculate how many blocks we need to wait
    while(true)
        if(start_index > xL || end_index > xL)
           break; 
        end
        
        %% Filtering Algorithm
        tic;
        x_block = x(start_index:end_index);
        x_fft = fft([x_block zeros(1,N)],N);
        
        yt = x_fft .* H;
        yt = ifft(yt,N);
        
        start_index = (end_index - M1)+1;
        end_index = end_index + L;
        filter_run_times(end+1) = toc;
        
        %% For the detection and display process
        new_x_block = x_block(M1+1:end);
        segment_line = [segment_line new_x_block];
        signal_chunks{end+1} = new_x_block;
        peaks = [];
        valleys = [];
        csg= [];
        cs_bs =[];
        
        %
        % N.B. Only start storing and processing after we have 2 chunks of the
        %      trend approximation. We discard the first to since they equate to
        %      M1/2 samples which is the phase/group delay for the FIR filter.
        %      Thoughts: Could these functions perhaps go in a class?
        % BOTTLE-NECK IS HERE
        if(counter > wait_period)
            new_y_block = yt(M1+1:end);
            soften_trend_line = [soften_trend_line new_y_block];
            
            trend_chunks{end+1} = new_y_block;
            FIR_chunk = signal_chunks{counter - floor((M1/2)/L)} - trend_chunks{end};
            soften_line = [soften_line FIR_chunk] %% Bottle-neck point
             
            % Resize if the line gets out of hand
            if(length(soften_trend_line) > original_xL)
                soften_trend_line = soften_trend_line(1:original_xL);
                segment_line = segment_line(1:original_xL);
                soften_line = soften_line(1:original_xL);
            end     
            
            % Ensure that we have enough data in order to start the
            % detection algorithm's calibration process. We select about 5
            % seconds worth of data in order to makesure we have enough
            % features for calibration.
            if(length(soften_line) >= 1500)
                [detector,curr_valleys,curr_peaks] = detector.DetectParse(soften_line);
                
                % Store the filtered chunks for later processing by the
                % cubic spline interpolation. 
                if(first_flag == true)
                    comb_valleys = curr_valleys(1:end-1,:);
                    comb_ind = 1:comb_valleys(end,1);
                    comb_seg = soften_line(comb_ind);
                    
                    [csg,cs_bs,cbs_interpolate_times(end+1)] = CubicSplineInterp(comb_seg,comb_valleys,comb_ind);
                    cubic_filt_line = [cubic_filt_line csg];
                    cubic_est_line = [cubic_est_line cs_bs];
                    start_point = comb_valleys(end,1);
                    
                    pre_valleys = curr_valleys;
                    first_flag = false;
                else
                    if(~isempty(curr_valleys))                      
                       % Check to see if we had obtained a valley in the
                       % detection to use as a start point
                       if(isnan(start_point))
                           comb_valleys = [pre_valleys(end,:); curr_valleys];
                           comb_ind = comb_valleys(1,1):length(soften_line);
                           comb_seg = soften_line(comb_ind);
                       else% In the case where we were able to get a valley for a start point
                           comb_valleys = [pre_valleys(end-1:end,:); curr_valleys];
                           comb_ind = start_point+2:length(soften_line);
                           comb_seg = soften_line(comb_ind);
                           start_point = NaN; 
                       end
                       
                       % CBS Interpolate the current segment
                       [csg,cs_bs,cbs_interpolate_times(end+1)] = CubicSplineInterp(comb_seg,comb_valleys,comb_ind);
                       cubic_filt_line = [cubic_filt_line csg];
                       cubic_est_line = [cubic_est_line cs_bs];
                       pre_valleys = curr_valleys;
                       
                       % Clear the remainder of the cubic lines up to the
                       % end for over-write with the next segement. We're
                       % only interested in merging at the valleys since
                       % it's all equal to 0
                       cubic_filt_line(pre_valleys(end,1):end) = [];
                       cubic_est_line(pre_valleys(end,1):end) = [];
                    end
                end
            end
        end

        % Live debug mode display
        if(live == true)        
            peaks = detector.GetPeaks();
            valleys = detector.GetValleys();
            FFCFLiveDisplay(segment_line,soften_trend_line,soften_line,peaks,valleys,cubic_est_line,cubic_filt_line);
        end
        counter = counter + 1;
    end
    
    % Since the end may not always has a valley, we
    % take the second last valley, the last valley and the length of the
    % signal from the second last valley to the last valley and process it
    % as an individual then merge it to give the final signal.
    %
    % This shouldn't really be much of an issue for real-time processing
    % but it's nice to have a complete overview without any loss of data
    % when the filtering process is stopped by the user or it's conditions.
    
    last_valleys = detector.GetValleys();
    last_valleys = last_valleys(end-1:end,:);
    
    
    % House keeping: In order to make sure that the approximation is even we
    % need to add a dummy value to the cubic spline estimator
    dummy_range = length(soften_line) - last_valleys(1,1);
    
    dummy_valley = [last_valleys(end,1)+dummy_range,last_valleys(end,2)+0.5];
    last_valleys = [last_valleys; dummy_valley];
    rem_ind =  last_valleys(1,1):length(soften_line);
    
    remaining_segment = soften_line(last_valleys(1,1):length(soften_line));
    [rem_csg,rem_cbs,cbs_interpolate_times(end+1)] = CubicSplineInterp(remaining_segment,last_valleys,rem_ind);
    
    cubic_filt_line(rem_ind) = rem_csg;
    cubic_est_line(rem_ind) = rem_cbs;
   
    % Map the new peak/valley locations to the final conditioned signal
    peaks = detector.GetPeaks();
    valleys = detector.GetValleys();
    
    mapped_peaks = [peaks(:,1) cubic_filt_line(peaks(:,1))'];
    mapped_valleys = [valleys(:,1) cubic_filt_line(valleys(:,1))'];
    
    % Update the display
    FFCFLiveDisplay(segment_line,soften_trend_line,soften_line,peaks,valleys,cubic_est_line,cubic_filt_line);
    
    % Handle post filtering stuff for display and finalization of results
    if(~isempty(cubic_est_line))
        if(length(soften_trend_line) >= length(cubic_est_line))
            combined_trend = soften_trend_line(1:length(cubic_est_line)) + cubic_est_line;
        else
            combined_trend = cubic_est_line(1:length(soften_trend_line)) + soften_trend_line;
        end
    end
  
    assignin('base','filter_run_times',filter_run_times);
    assignin('base','detection_run_times',detector.GetRunTimes());
    assignin('base','cbs_interpolate_times',cbs_interpolate_times);
    
    GenerateRuntimeReport(filter_run_times,detector.GetRunTimes(),cbs_interpolate_times);
end

function GenerateRuntimeReport(filter_run_times,detector_times,cbs_time)
    disp('****************************************');
    disp(['FFCF ALGORITHM RUN-TIME REPORTS: ']);
    disp('****************************************');
    disp(['Mean Filter Time: ' sprintf('%f',mean(filter_run_times))]);
    disp(['Mean ADS Time: ' sprintf('%f',mean(detector_times))]);
    disp(['Mean CBS Time: ' sprintf('%f',mean(cbs_time))]);
end

function [csg,bs,ti] = CubicSplineInterp(segment, valleys,ind)
    tic;
    spline_est = pchip(valleys(:,1),valleys(:,2));
    bs = fnval(ind,spline_est);
    
    if(length(bs) > length(segment))
        bs = bs(1:length(length(segment)));
        csg = segment - bs;
    else
        csg = segment - bs;
    end
    ti = toc;
end

function FFCFLiveDisplay(segment_line,trend_line,filt_line,...
                     peaks,valleys,cubic_est_line,cubic_filt_line)
    figure(4);
    set(gcf, 'Position', [100 100 10*100, 5.5*100]); %<- Set size
    set(gcf,'Renderer','opengl');
    clf;
    subplot(2,2,1);
    set(gca, 'FontSize', 13, 'LineWidth', 0.8,'FontName','Helvetica'); %<- Set properties
    hold on;
    box on;
    plot(segment_line,'LineWidth',1.1,'Color','k');
    plot(trend_line,'r','LineWidth',1.1);
    title('FIR drift approximation');

    subplot(2,2,3);
    set(gca, 'FontSize', 13, 'LineWidth', 0.8,'FontName','Helvetica'); %<- Set properties
    hold on;
    box on;
    plot(filt_line,'LineWidth',1.1,'Color','k');
    title('Features Detected after FIR filtering');
    if(~isempty(peaks))
        plot(peaks(:,1), peaks(:,2), 'ro','MarkerFaceColor','r');
    end

    if(~isempty(valleys))
        plot(valleys(:,1), valleys(:,2), 'bo','MarkerFaceColor','b');
    end

    plot(cubic_est_line,'LineWidth',1.1,'Color','r','LineStyle','-');

    subplot(2,2,4);
    set(gca, 'FontSize', 13, 'LineWidth', 0.8,'FontName','Helvetica'); %<- Set properties
    hold on;
    box on;
    plot(cubic_filt_line,'LineWidth',1.1,'Color','k');
    title('Cubic Spline Filtered Signal');

    subplot(2,2,2);
    set(gca, 'FontSize', 13, 'LineWidth', 0.8,'FontName','Helvetica'); %<- Set properties
    hold on;
    box on;
    plot(segment_line,'LineWidth',1.1,'Color','k');
    if(~isempty(cubic_est_line))

        if(length(trend_line) >= length(cubic_est_line))
            plot(trend_line(1:length(cubic_est_line)) + cubic_est_line,'r','LineWidth',1.1,'LineStyle','-');
        else
            plot(cubic_est_line(1:length(trend_line)) + trend_line,'r','LineWidth',1.1,'LineStyle','-');
        end
    end
    title('Combined Baseline Drift Approximation');

    drawnow;
    %             pause(0.1); % Delay so we can observe it better
end