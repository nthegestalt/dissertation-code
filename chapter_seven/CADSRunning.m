%% File: CADSRunning
%
%  Description: Implements the ADS algorithm for segmentation based
%               parsing. This is the continuous variant of the 
%               CAdaptiveSegmentationV2 class implementation.
%
%  Author: Tuan
%  Last modified: 22nd October 2015
%

classdef CADSRunning < handle
    properties
        amplitude_low;
        amplitude_high;
        amp_low_slow;
        amp_low_fast;
        amp_high_slow;
        amp_high_fast;
        first_parse;
        average_amplitude;
        distance_avg;
        isOutofRange;
        next_estimated_valley;
        last_valley;
        current_index;        
        back_counter;
        front_counter;
        steps;
        peaks;
        valleys;
        segment_collection;
        line_collection;
        end_valley;
        end_peak;
        candidate_segment;
        segment_runtimes;
    end
    
    methods
        %% Constructor
        %
        %  Instantiates the class values
        %  Param: Nil
        function self = CADSRunning()
            self.amplitude_low = 0;
            self.amplitude_high = 0;
            self.amp_low_slow = 0.5;
            self.amp_low_fast = 0.8;
            self.amp_high_slow = 1.4;
            self.amp_high_fast = 2.0;
            self.first_parse = true;
            self.average_amplitude = 0;
            self.distance_avg = 0;
            self.next_estimated_valley = 0;
            self.last_valley = [];
            self.isOutofRange = 0;
            self.current_index = 1;
            self.back_counter = 1;
            self.front_counter = 2;
            self.steps = 1;
            
            self.peaks = [];
            self.valleys = [];
            self.segment_collection = [];
            self.end_valley = [];
            self.end_peak = [];
            self.line_collection = [];
            self.candidate_segment = [];
            self.segment_runtimes = [];
        end
        
        %% DetectParse
        %
        %  Desc: Parses the segments and detect the features
        %
        %  Param: current segment, previous segment
        %  Return: segment peaks, segment valleys
        %
        function [self,seg_valleys,seg_peaks] = DetectParse(self,line_segment)
            tic;
            % Construct containers
            segment_length = length(line_segment);
            seg_valleys = [];
            seg_peaks = [];
            
            % Initialise variables
            max_amplitude = -inf;
            
            curr_back_counter = self.back_counter;
            curr_front_counter = self.front_counter;
            
            % 1. Start the construction of the line segment
            if(isempty(self.line_collection))
                self.line_collection = [curr_back_counter, line_segment(curr_back_counter),...
                                    curr_front_counter, line_segment(curr_front_counter)];
            else
                 curr_back_counter = curr_back_counter - 4;
                 curr_front_counter = curr_front_counter - 4;
            end
                           
            while(true)
                % Counter increment
                curr_back_counter = curr_back_counter + self.steps;
                curr_front_counter = curr_front_counter + self.steps;
                
                % Loop Exit Condition
                if(curr_front_counter >= segment_length || ...
                        curr_back_counter >= segment_length)
                   break; 
                end
                
                % 2. Build a new line from the points in the signal
                current_line = [curr_back_counter, line_segment(curr_back_counter) ...
                                curr_front_counter, line_segment(curr_front_counter)];
            
                % 3. Directional slope check to see if the current and
                %    previous lines are in the same direction
%                 previous_slope = self.CheckSlope(self.line_collection(end,1),self.line_collection(end,3),...
%                                                  self.line_collection(end,2),self.line_collection(end,4));
                previous_slope = 0;
                if((self.line_collection(end,3)> self.line_collection(end,1)) ...
                        && (self.line_collection(end,4) > self.line_collection(end,2)))
                    previous_slope = 1;
                else
                    previous_slope = 0;
                end
%                 current_slope = self.CheckSlope(current_line(1,1),current_line(1,3),...
%                                                 current_line(1,2),current_line(1,4));
                current_slope = 0;
                if((current_line(1,3)> current_line(1,1)) ...
                        && (current_line(1,4) >current_line(1,2)))
                    current_slope = 1;
                else
                    current_slope = 0;
                end
                
                if(current_slope > 0 && previous_slope > 0)
                    % 3.1. Merge the lines into one and update the
                    %      candidate segment with the values
                    self.line_collection = [self.line_collection ; current_line];
                    self.candidate_segment = [self.line_collection(1,1) self.line_collection(1,2) ...
                                         current_line(1,3) current_line(1,4)];  
                    
                else
                    % 4. In the case of a negative slope close it off,
                    %    ship it and start a new segment
                    self.line_collection = current_line;
                    
                    if(~isempty(self.candidate_segment))
                        % 5. If this is our first run we're running the
                        %    calibration procedure
                        if(self.first_parse == true)
                            self.segment_collection = [self.segment_collection;self.candidate_segment];
                            current_amplitude = (self.candidate_segment(4) - self.candidate_segment(2));
                            
                            % 5.1. Need max amplitude for initial
                            %      discrimination procedures
                            if(current_amplitude > max_amplitude)
                                max_amplitude = current_amplitude;
                            end
                            
                            num_of_segments = size(self.segment_collection);
                            num_of_segments = num_of_segments(1);
                            
                            % 5.2. Once we have enough segments we can start
                            %      the discrimination process
                            if(num_of_segments >= 5)
                                [self] = self.CalibrationRoutine(max_amplitude);
                                
                                % 5.3. Get the peaks and valleys from the
                                %      start/end of the line segments
                                self.peaks = [self.segment_collection(:,3),self.segment_collection(:,4)];
                                self.valleys = [self.segment_collection(:,1),self.segment_collection(:,2)];
                                
                                seg_valleys = self.valleys;
                                seg_peaks = self.peaks;
                                
                                self.end_valley = self.valleys(end,:);
                                self.end_peak = self.peaks(end,:);
                            end % END CALIBRATION-PHASE
                        else
                            % 6. Perform normal segmentation and
                            %    discrimination procedures
                            current_amplitude = (self.candidate_segment(4) - self.candidate_segment(2));
                            
                            % 6.1. If the current amplitude is in range,
                            %      add it to the collection and get the
                            %      peaks/valleys from the points
                            if(current_amplitude >= self.amplitude_low ...
                                && current_amplitude <= self.amplitude_high)
                                self.segment_collection = [self.segment_collection;self.candidate_segment];
                                
                                % Peak Check
                                curr_peak = self.PeakCheckProcedure(line_segment, self.candidate_segment);
                                self.peaks = [self.peaks; curr_peak];
                                seg_peaks = [seg_peaks;curr_peak];
                                
                                % Valley Check
                                curr_valley = self.ValleyCheckProcedure(line_segment,self.candidate_segment);
                                self.valleys = [self.valleys; curr_valley];
                                seg_valleys = [seg_valleys;curr_valley];
                                
                                % Update algorithm parameters
                                self.amplitude_low = (self.amplitude_low + current_amplitude*0.6)/2;
                                self.amplitude_high = self.amplitude_high + (current_amplitude*1.2);
                                self.average_amplitude = (self.average_amplitude + current_amplitude) / 2;
                                self.isOutofRange = 0;
                            else
                                % 7. Sometimes due to the nature of the algorithm real segments will actually be
                                %    skipped due to the adaptive threshold adjustments. To make sure it isn't we
                                %    double check it to see if the amplitude of the segment matches to that of the
                                %    average values.
                                isReallyOutOfRange = 1;
                                avg_amp_low = self.average_amplitude * 0.5;
                                avg_amp_high = self.average_amplitude* 2.2;
                                if(current_amplitude >= avg_amp_low && current_amplitude <= avg_amp_high)
                                    self.segment_collection = [self.segment_collection; self.candidate_segment];
                                    
                                    % Peak Check
                                    curr_peak = self.PeakCheckProcedure(line_segment, self.candidate_segment);
                                    self.peaks = [self.peaks; curr_peak];
                                    seg_peaks = [seg_peaks; curr_peak];
                                    
                                    % Valley Check
                                    curr_valley = self.ValleyCheckProcedure(line_segment,self.candidate_segment);
                                    self.valleys = [self.valleys; curr_valley];
                                    seg_valleys = [seg_valleys; curr_valley];
                                    
                                    self.average_amplitude = (self.average_amplitude + current_amplitude) / 2;
                                    isReallyOutOfRange = 0;
                                end%% END AMP-AVG CHECK
                                
                                % 7.1. Error flag raised: modify threshold to
                                %      include greater expanse
                                if(self.isOutofRange > 0)
                                    self.amplitude_low = (self.amplitude_low + current_amplitude*0.8)/2;
                                    self.amplitude_high = (current_amplitude*2.0);
                                end %% END OFR CHECK
                                
                                % 7.2. Raise Error flag
                                if(isReallyOutOfRange == 1)
                                    self.isOutofRange = self.isOutofRange + 1;
                                end % END RORF CHDCK
                            end %% END REGULAR AMP-CHECK
                        end %% END CALIBRATION-CHECK
                        self.candidate_segment = []; % Clear segment          
                    end %% END EMPTY-CHECK
                end %% END SLOPE CHECK
            end 
            
            ti = toc;
            self.segment_runtimes(end+1) = ti;
            
            self.back_counter = curr_back_counter;
             self.front_counter = curr_front_counter;
            
            % Add the new set of peaks and valleys to the list
            if(~isempty(self.peaks))
                self.end_peak = self.peaks(end,:);
            end
            
            if(~isempty(self.valleys))
                self.end_valley = self.valleys(end,:);
            end

        end
        
        %% CalibrationRoutine
        %
        %  Desc: Initialises the algorithm parameters through calibration routine
        %
        %  Param: segments, max amplitude
        %  Return: updated segement collection
        %
        function [self] = CalibrationRoutine(self,max_amplitude)
            updated_segment_collection = [];
            % We have a bunch of segments so we need to determine which
            % segments are real and which are fakes.
            self.amplitude_low = max_amplitude * self.amp_low_slow;
            self.amplitude_high = max_amplitude * self.amp_high_slow;
            
            for(i = 1:size(self.segment_collection))
                current_segment = self.segment_collection(i,:);
                current_amplitude = (current_segment(4) - current_segment(2));
                
                if(current_amplitude >= self.amplitude_low && current_amplitude <= self.amplitude_high)
                    updated_segment_collection = [updated_segment_collection; current_segment];
                end
            end
            self.segment_collection = updated_segment_collection;
            
            self.first_parse = false;
            end_segment = self.segment_collection(end,:);
            end_amplitude = end_segment(4) - end_segment(2);
            
            % Initialise values for parsing next segments
            self.amplitude_low = end_amplitude * 0.5;
            self.amplitude_high = end_amplitude * 2.0;
            self.average_amplitude = end_amplitude;
            self.distance_avg = mean(diff(self.segment_collection(:,1)));          
        end        
        
        %% PeakCheckProcedure
        %
        % Desc: Check if this peak is a replicant or real. 
        %       There is a displacement of 20 to accomodate 
        %       filtering result differences.
        %
        % Param: candidate_segment
        % Return: Peak, Number of true peaks
        %
        function [peak] = PeakCheckProcedure(self, line_segment, candidate_segment)
            peak = [];
            pre = line_segment(candidate_segment(:,3)-1);
            curr = line_segment(candidate_segment(:,3));
            next = line_segment(candidate_segment(:,3)+1);
            
            max_check = CheckInflection('max',curr,pre,next);
            if(max_check == true)
                peak = [candidate_segment(3), candidate_segment(4)];
            end
        end
        
        %% ValleyCheckProcedure
        %
        % Desc: Check if this valley is a replicant or real. 
        %       There is a displacement of 20 to accomodate 
        %       filtering result differences.
        %
        % Param: candidate_segment
        % Return: Valley, Number of True Valleys
        %
        function [valley] = ValleyCheckProcedure(self,line_segment,candidate_segment)
            valley = [];
            pre = line_segment(candidate_segment(:,1)-1);
            curr = line_segment(candidate_segment(:,1));
            next = line_segment(candidate_segment(:,1)+1);
            
            min_check = CheckInflection('min',curr,pre,next);
            if(min_check == true)
                % Search frowards/back a small amount to see if this is the valley
                % we're looking for or if it's false one in the cases of
                % multiple valleys
                
                for(i = candidate_segment(:,1):-1:candidate_segment(:,1)-75)
                    if(line_segment(i) < curr)
                        candidate_segment(1) = i;
                        candidate_segment(2) = line_segment(i);
                    end
                end
                valley = [candidate_segment(1), candidate_segment(2)];
            end
        end
        
        %% AUXILLARY
        %% CheckSlope
        %
        %  Desc: Determines if the slope is rising or falling
        %        Note: Do NOT use this. It will slow the code down
        %        immensely due to MATLAB's hatred of functions.
        %
        %  Param: line co-ordinates
        %  Return: true/false
        %
        function is_upwards_slope = CheckSlope(self,x1,x2,y1,y2)
            if((x2 > x1) && (y2 > y1))
                is_upwards_slope = 1;
            else
                is_upwards_slope = 0;
            end
        end
        
        %% GetPeaks
        %
        %  Desc: Gets the peaks collected over the detection period
        %
        %  Param: Nil
        %  Return: Peaks
        %        
        function peaks = GetPeaks(self)
           peaks = self.peaks; 
        end
        
        %% GetValleys
        %
        %  Desc: Gets the valleys collected over the detection period
        %
        %  Param: Nil
        %  Return: Valleys
        %           
        function valleys = GetValleys(self)
           valleys = self.valleys; 
        end
        
        %% GetSegments
        %
        %  Desc: Gets the segments collected over the detection period
        %
        %  Param: Nil
        %  Return: Valleys
        %           
        function segments = GetSegments(self)
           segments = self.segment_collection; 
        end
        
        %% GetRunTImes
        %
        %  Desc: Gets the run times for processing each segment
        %
        %  Param: Nil
        %  Return: RunTimes
        %
        function run_times = GetRunTimes(self)
           run_times = self.segment_runtimes; 
        end
    end
    
end

