%% Filename: CalculateThreshold	
%
% Description: Calculates the change rate values for the ADT slope lines
% Author: Tuan
% Last Modified: 5th February 2016
% 
% Params: line type, current amplitude, previous amplitude, 
%         standard deviation, sampling frequency
%
% Return: Line threshold
%  
function threshold ...
    = CalculateThreshold( type, slope_amplitude, previous_amplitude, ...
                           std_dev, sampling_freq )
  if(type == 'min')
      threshold = slope_amplitude + (1.5 * (previous_amplitude + std_dev)/ sampling_freq);
  elseif(type == 'max')
      threshold = slope_amplitude + (-0.6 * (previous_amplitude + std_dev)/ sampling_freq);
  end
end

