%% Filename: CheckIntersect
%
% Description: Checks for intersection between two lines/vectors for the ADT slope lines
% Algorithm Referenced from:
%			- Cormen, T. H. (2001). Introduction to algorithms (Vol. 2nd). 
%				Cambridge, Mass;London;: MIT Press.
%
%			- http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
%
% Author: Tuan
% Last Modified: 5th February 2016
% 
% Params: Line 1 points (start/end), Line 2 points (start/end)
%
% Return: True/False
%  
function [ intersect ] = CheckIntersect( p1, q1, p2, q2)
   %Debug flag
   debug_flag = false;
   
   % Find the orientations for the lines
   o1 = calculate_orientation(p1,q1,p2);
   o2 = calculate_orientation(p1,q1,q2);
   o3 = calculate_orientation(p2,q2,p1);
   o4 = calculate_orientation(p2,q2,q1);
   
   % General case
   if(o1 ~= o2 && o3 ~= o4)
       intersect = true;
       if(debug_flag == true)
            disp('General');
            disp('Intersects');
       end
       return;
   end
   
   % Line segment
   segment = 0;
   
   % Special Cases
   % p1, q1 and p2 are co-linear and p2 lies on segment p1q1
   segment = onSegment(p1,p2,q1);
   if(o1 == 0 && segment)
      intersect = true;
      if(debug_flag == true)   
          disp('Intersects');
      end
      return;
   end
   
   % p1, q1 and p2 are co-linear and q2 lies on segment p1q1
   segment = onSegment(p1,q2,q1);
   if(o2 == 0 && segment)
      intersect = true;
      disp('Intersects');
      return;
   end
   
   % p2, q2 and p1 are co-linear and p1 lies on segment p2q2
   segment = onSegment(p2,p1,q2);
   if(o3 == 0 && segment)
      intersect = true;
      if(debug_flag == true)   
          disp('Intersects');
      end
      return;
   end
   
   % p2, q2 and q1 are co-linear and q1 lies on segment p2q2
   segment = onSegment(p2,q1,q2);
   if(o4 == 0 && segment)
      intersect = true;
      if(debug_flag == true)   
          disp('Intersects');
      end
      return;
   end
   
   intersect = false;
   if(debug_flag == true)   
         disp('Intersects');
   end
   return;
end

%% calculate_orientation
%
%  Desc: Calculates the orientation of the lines see their alignment
%
%  Param: line1(start/end), line2(start/end)
%  Return: orientation(0 = co-linear, 1 = clockwise, 2 = anti-clockwise)
%
function [orientation] = calculate_orientation(p,q,r)
    val = (q(1,2) - p(1,2)) * (r(1,1) - q(1,1)) - (q(1,1) - p(1,1)) * (r(1,2) - q(1,2));
    
    if(val == 0)
        orientation = 0;
        return;
    else
       if(val > 0)
          orientation = 1;
          return;
       else
          orientation = 2;
          return;
       end
    end
end

%% onSegment
%
%  Desc: Checks whether a point of a line is lying on another segment
%
%  Param: line1(start/end), line2(start/end)
%  Return: true/false
%
function [isOnSegment] = onSegment(p,q,r)
    % If we have 3 colinear points p,q,r, check if point q lies on the
    % line segment pr
    maxValOne = det_max(p(1,1), r(1,1));
    minValOne = det_min(p(1,1), r(1,1));
    maxValTwo = det_max(p(1,2), r(1,2));
    minValTwo = det_min(p(1,2),r(1,2));
    
    if((q(1,1) <= maxValOne && q(1,1) >= minValOne)...
        && (q(1,2) <= maxValTwo && q(1,2) >= minValTwo))
        isOnSegment = true;
        return;
    else
        isOnSegment = false;
        return;
    end
end

%% det_max
%
%  Desc: Selects largest value from two given values
%
%  Param: value 1, value 2
%  Return: max value
%
function [maxVal] = det_max(v1,v2)
    if(v1 < v2)
        maxVal = v2;
        return;
    else
        maxVal = v1;
        return;
    end
end

%% det_min
%
%  Desc: Selects smallest value from two given values
%
%  Param: value 1, value 2
%  Return: min value
%
function [minVal] = det_min(v1,v2)
    if(v1 < v2)
        minVal = v1;
        return;
    else
        minVal = v2;
        return;
    end
end
