%% Filename: CheckInflection
%
% Description: Checks whether the next point is an inflection point or not 
% Author: Tuan
% Last Modified: 5th February 2016
% 
% Params: type, current point, previous point, next point
%
% Return: True/False
%  
function isInflection = CheckInflection( type, curr, pre, next )
    if(type == 'max')            
        if(pre < curr && curr >= next)
           isInflection = 1;
        else
            isInflection = 0;
        end
        
    elseif(type == 'min')        
        if((pre > curr && curr <= next) || (pre >= curr && curr <= next))
            isInflection = 1;
        else
            isInflection = 0;
        end

    else
        disp('Invalid inflection type.');
        isInflection = false;
        return;
    end
end

