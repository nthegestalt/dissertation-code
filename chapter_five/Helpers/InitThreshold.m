%% Filename: InitThreshold
%
% Description: Initialises the change rate values for the ADT slope lines
% Author: Tuan
% Last Modified: 5th February 2016
% 
% Params: type, signal line
%
% Return: line threshold
%  
function threshold = InitThreshold(type, signal)
    % Calculates the initial threshold for starters
    if(type == 'max')
        max_val = max(signal);
        threshold = max_val - 0.2*( max_val);
    elseif(type == 'min')
        min_val = min(signal);
        
        threshold = min_val + (0.2*( min_val));
    else
        threshold = signal(1);
        disp('Init Threshold: Error. Invalid type.');
    end
    
    return;
end
