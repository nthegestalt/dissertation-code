%% Filename: MovingStdDev
%
% Description: Actually calculates the variance, but by sqrt(variance) 
%							 we can get the standard deviation.
% Author: Tuan
% Last Modified: 5th February 2016
% 
% Params: counter, current mean, old sum of square differences from mean, current signal value
%
% Return: std. deviation, new mean, new sum of square differences from mean
%  
function [ stddev, new_mean, new_M2 ] = MovingStdDev( counter, mean, M2, sig_value )
    if(counter < 2)
        stddev = 0;
        new_mean = 0;
        new_M2 = 0;  
        return;
    end
    
    delta = sig_value - mean;
    new_mean = mean + delta/counter;
    new_M2 = M2 + delta * (sig_value - new_mean);
    
    variance = new_M2 / (counter - 1);
    stddev = sqrt(variance);
    
end

