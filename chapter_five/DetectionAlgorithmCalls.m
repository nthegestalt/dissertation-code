%% File: DetectionAlgorithmCalls
%
%  Description: Calls each of the algorithms for analysis
%
%  Author: Tuan
%  Last modified: 5th February 2016
%

function DetectionAlgorithmCalls
	% Parameters
	file_name = '0028_8min';
	sample_file = [file_name '.mat'];
	load(sample_file);
	start = 1;
	sample_length = 6000;

	% Load and prepare the signal
	ppg_signal = (signal.pleth.y(start:sample_length));
	ppg_signal = tsmovavg(ppg_signal, 's', 30,1);
	ppg_signal(1:30) = ppg_signal(30);

	[fir_csg,fir_bs,fir_ti] = FIRFFTFilter(ppg_signal);

    % Lisheng WCF
%     [wv_csg,wv_bs,wv_ti] = WaveletCascadedFilter(ppg_signal',8);
%     CallWCFAlgorithm(wv_csg,file_name);
    
    % Kan IIR-ZPF
%     CallKanAlgorithm(fir_csg,file_name);
    
	% ADT
% 	CallADTAlgorithm(fir_csg,file_name)
    
    % ADS
%     CallADSAlgorithm(fir_csg,file_name);
end

function CallWCFAlgorithm(wv_csg,file_name)
    [derivative,deriv_peaks,valleys, peaks,ti]= WaveletCascadedDetect(wv_csg);
    DisplayWaveletCascadeDet(derivative,deriv_peaks,wv_csg,peaks,valleys,file_name,false);
    GenerateDetectionReport(peaks,valleys,ti,file_name,'WCF');
end

function CallKanAlgorithm(fir_csg,file_name)
    tic;
    [valleys_a,peaks_a,missed_peaks_distance, ...
        missed_peaks_height, missed_valleys_height] = KanIIRZPFDetect(fir_csg);
    ti = toc;
    
    DisplayKanIIRZPF(fir_csg,valleys_a,peaks_a,missed_peaks_distance,...
                          missed_peaks_height,missed_valleys_height,file_name, false);
    GenerateDetectionReport(peaks_a,valleys_a,ti,file_name,'ADT');
end

function CallADTAlgorithm(fir_csg,file_name)
	fir_csg_nm = fir_csg + abs(min(fir_csg));

	tic;
	[max_slope,min_slope, peaks,valleys] = ADT(fir_csg_nm, 300, false);
	ti = toc;
	DisplayADT(fir_csg_nm,max_slope,min_slope, peaks,valleys);
    GenerateDetectionReport(peaks,valleys,ti,file_name,'ADT');
end

function CallADSAlgorithm(fir_csg,file_name)
    [peaks,valleys,line_segments,ti] = ADSDetect(fir_csg);
    DisplayADS(fir_csg,peaks,valleys,line_segments);
    GenerateDetectionReport(peaks,valleys,ti,file_name,'ADS');
end

