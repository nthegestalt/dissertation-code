%% File: ADSDetect
%
%  Description: Implements a modified version of the ADS Algorithm by Karlen et al. 2012.
%               Paper Reference:
%               
%               -------------------------------------------------------------------------------------------------------------
%               Karlen, W., Ansermino, J. M., & Dumont, G. (2012, Aug. 28 2012-Sept. 1 2012). 
%               Adaptive pulse segmentation and artifact detection in photoplethysmography 
%								for mobile applications.
%
%               Paper presented at the Engineering in Medicine and Biology Society (EMBC), 
%               2012 Annual International Conference of the IEEE.
%               -------------------------------------------------------------------------------------------------------------
%								 
%
%  Author: Tuan
%  Last modified: 18th December 2015
%
%  Param: signal
%  Return: peaks, valleys, segments, run time
function [peaks,valleys,segment_collection,ti] = ADSDetect(signal)
tic;
    % Segmentation parameters
    back_counter = 1;
    front_counter = 2;
    m = 1;
    signal_length = length(signal);
    line_collection = [back_counter signal(back_counter) front_counter signal(front_counter)]; % Make the first line segment
    segment_collection = [];
    candidate_segment = [];
    peaks = [];
    valleys = [];
    
    % Adaptive Threshold Parameters
    Amplitude_low = 0;
    Amplitude_high = 0;
    is_inRange = true;
    first_parse = true;
    max_amplitude = -inf;
    amplitude_shortlist = [];
    average_amplitude = 0;
    
    last_valley = 0;
    
    % Loop through the signal to make the line segments
    while true
        back_counter = (back_counter) + m;
        front_counter = front_counter + m;
        
        if(front_counter >= signal_length || back_counter >= signal_length)
            break;
        end
        
        curr_line = [back_counter signal(back_counter) front_counter signal(front_counter)];
        
        % If we have more than two lines in our collection, get the slopes
        % of the lines todetermine whether they're in the same direction for merging
        slope_pre = 0;
        slope_curr = 0;
        
        % Check the slope direction
        if(line_collection(end,3)>line_collection(end,1) ...
                && line_collection(end,4) > line_collection(end,2))
           slope_pre = 1; 
        end
        
        if(curr_line(1,3)>curr_line(1,1) ...
                && curr_line(1,4) > curr_line(1,2))
            slope_curr = 1;
        end
        
        % If they're not in the same direction, discard, close off and start a new segment
        if(slope_curr > 0 && slope_pre > 0)
            line_collection = vertcat(line_collection, curr_line);
            candidate_segment = [line_collection(1,1) line_collection(1,2) line_collection(end,3) line_collection(end,4)];
        else       
            % Empty the collection and start a new one
            line_collection = [];
            line_collection = curr_line;
            
            % If the candidate segment isn't empty, add it to the
            % segment collection
            if(~isempty(candidate_segment))
                current_amplitude = (candidate_segment(4) - candidate_segment(2));
                if(current_amplitude > max_amplitude)
                    max_amplitude = current_amplitude;
                end
                
                if(first_parse == true)
                    segment_collection = vertcat(segment_collection, candidate_segment);
                    amplitude_shortlist = [amplitude_shortlist current_amplitude];
                    temp_segments = [];
                    seg_count = size(segment_collection);
                    seg_count = seg_count(1);
                    
                    if(seg_count > 5)
                        Amplitude_low = (Amplitude_low + max_amplitude*0.6)/2;
                        Amplitude_high = Amplitude_high + (max_amplitude*1.4);
                        
                        for(i = 1:seg_count)
                           curr_segment =  segment_collection(i,:);
                           
                           if(amplitude_shortlist(i) >= Amplitude_low && amplitude_shortlist(i) <= Amplitude_high)
                               % Search back to identify the real min
                               for(i = curr_segment(1):-1:curr_segment(1)-75)
                                   if(i <= 0)
                                      break; 
                                   end
                                   if(signal(i) < curr_segment(2))
                                       curr_segment(1) = i;
                                       curr_segment(2) = signal(i);
                                   end
                               end
                               temp_segments = vertcat(temp_segments, curr_segment);
                           end
                        end
                        segment_collection = temp_segments;
                        
                        peaks = [segment_collection(:,3) segment_collection(:,4)];
                        valleys = [segment_collection(:,1) segment_collection(:,2)];
                        last_valley = valleys(end,1);
                        
                        first_parse = false;
                        end_segment = temp_segments(end,:);
                        end_amplitude = end_segment(4) - end_segment(2);
                        Amplitude_low = end_amplitude * 0.5;
                        Amplitude_high = end_amplitude * 2.0;
                        average_amplitude = end_amplitude;
                    end
                else 
                    % Check to see if segment slope is within range. If
                    % it is add it to the collection and recalibrate the
                    % thresholds otherwise increment the error flag
                    if(current_amplitude >= Amplitude_low && current_amplitude <= Amplitude_high)
                        for(i = candidate_segment(1):-1:candidate_segment(1)-75)
                            
                            if(i <= 0)
                               break; 
                            end
                            
                            if(signal(i) < candidate_segment(2))
                                candidate_segment(1) = i;
                                candidate_segment(2) = signal(i);
                            end
                        end
                        segment_collection = vertcat(segment_collection, candidate_segment);
                        
                        if(candidate_segment(1) ~= last_valley)
                            last_valley = candidate_segment(1);
                            valleys = [valleys; [candidate_segment(1) candidate_segment(2)]];
                        end
                        
                        peaks = [peaks; [candidate_segment(3) candidate_segment(4)]];
                        
                        Amplitude_low = (Amplitude_low + current_amplitude*0.6)/2;
                        Amplitude_high = (current_amplitude*1.4);
                        is_inRange = true;
                        
                        average_amplitude = (average_amplitude + current_amplitude)/2;
                    else
                        % Make sure it's really out of range based on the
                        % overall average of the amplitudes collected so
                        % far instead of only it's neighbors.
                        avg_Amplitude_low = (Amplitude_low + average_amplitude*0.5)/2;
                        avg_Amplitude_high = Amplitude_high + (average_amplitude*1.4);
                        is_ReallyOutofRange = true;
                        if(current_amplitude >= avg_Amplitude_low && current_amplitude <= avg_Amplitude_high)
                            for(i = candidate_segment(1):-1:candidate_segment(1)-75)
                                if(signal(i) < candidate_segment(2))
                                    candidate_segment(1) = i;
                                    candidate_segment(2) = signal(i);
                                end
                            end
                            
                            segment_collection = vertcat(segment_collection, candidate_segment);
                            
                            % If this isn't the previous valley/peak we added
                            % then add it to the collection
                            if(candidate_segment(1) ~= last_valley)
                                last_valley = candidate_segment(1);
                                valleys = [valleys; [candidate_segment(1) candidate_segment(2)]];
                            end
                            
                            peaks = [peaks; [candidate_segment(3) candidate_segment(4)]];
                            
                            Amplitude_low = (Amplitude_low + current_amplitude*0.6)/2;
                            Amplitude_high = (current_amplitude*1.4);
                            average_amplitude = (average_amplitude + current_amplitude)/2;
                            is_ReallyOutofRange = false;
                        end
                        
                        if(is_inRange == false)
                            Amplitude_low = (Amplitude_low + current_amplitude*0.8)/2;
                            Amplitude_high = (current_amplitude*2.0);
                        end
                        
                        if(is_ReallyOutofRange == true)
                            is_inRange = false;
                        end
                    end
                end
                candidate_segment = [];
            end
        end
    end
ti = toc;
end
