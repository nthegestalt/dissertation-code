%% File: WaveletCascadedDetect
%
%  Description: Detection algorithm presented in the Wavelet Cascaded
%               Paper Reference:
%               
%               -------------------------------------------------------------------------------------------------------------
%               Xu, Lisheng, Zhang, David, Wang, Kuanquan, Li, Naimin, & Wang, Xiaoyun. (2007). 
%               Baseline wander correction in pulse waveforms using wavelet-based cascaded adaptive filter. 
%               Computers in Biology and Medicine, 37(5), 716-731. '
%               doi: http://dx.doi.org/10.1016/j.compbiomed.2006.06.014
%               -------------------------------------------------------------------------------------------------------------
%
%  Author: Tuan
%  Last modified: 18th December 2015
%
%  Param: signal
%  Return: derivative, derivative peaks, valleys, peaks, run time
function [derivative,deriv_peaks, valleys,peaks,ti] = WaveletCascadedDetect(signal)
tic;
    % Calculate the first derivative
    y = [];
    for(i = 2:length(signal)-1)
        y(i) = (signal(i+1) - signal(i-1))/2;
    end
    
    % Zero the ends
    y(1) = 0;
    y(end) = 0;
    
    % Filter the first derivative for positive values only
    z = [];
    for(i = 1:length(y))
        if(y(i) > 0)
           z(end+1) = y(i);
        else
            z(end+1) = 0;
        end
    end
    
    z = filter(ones(10,1)/10,1,z);
    
    % Get the max for the threshold calculation
    window_size = 600;
    max = -inf;
    max_vals = [];
    for(i = 1:length(z))
        if(z(i) > max)
            max = z(i);
        end
        
        if(i == window_size)
           max_vals(end+1) = max;
           window_size = window_size + 600; 
        end
    end
    
    % Calculate the threshold for discrimination
    max_vals = sort(max_vals);
    
    if(length(max_vals) >= 6)
        max_vals = max_vals(1:6);
    else
        max_vals = max_vals(1:end);
    end
    amplitude_threshold = max_vals(1) - (max_vals(end-1) - max_vals(2));
    amplitude_threshold = amplitude_threshold * 0.70;
    
    % Search z for peaks that exist equal to or above the threshold
    deriv_peaks = [];
    deriv_peaks_indices = [];
    for(i = 1:length(z)-2)
        pre = z(i);
        curr = z(i+1);
        next = z(i+2);
        
        max_check = CheckInflection('max',curr, pre, next);        
        if(max_check == true)
            if(curr >= amplitude_threshold)
                deriv_peaks = vertcat(deriv_peaks, [i curr]);
            end
        end
    end

    valleys = [];
    valleys_indices = [];
    peaks = [];
    peaks_indices = [];
    last_valley = 0;
    last_peaks = 0;
    
    % Search backwards from each peak to find the start
    for(i = 1:size(deriv_peaks,1))
        curr_index = deriv_peaks(i,1);
        back_start = curr_index-170;
        forward_end = curr_index+160;
        
        % Search backwards to find valleys
        if(back_start < 0)
            back_start = 2;
        end
        for(j =curr_index:-1:back_start)
            pre = signal(j-1);
            curr =  signal(j);
            next = signal(j+1);
            min_check = CheckInflection('min',curr, pre, next);
            
            % Search back tends to have a tendency to include previous
            % valleys in the cases where there are distortion effects that
            % are clumped together. We'll need to check to see whether we
            % have these already or not.
            if(min_check && j~= last_valley)
                
                % We found a min but is this just a notch? Search back a
                % bit further to find the real value if it exists
                new_index = j;
                for(k = j:-1:back_start)
                    if(k-1 <=0)
                        break
                    end
                    
                    new_pre = signal(k-1);
                    new_curr = signal(k);
                    new_next = signal(k+1);
                    
                    new_min_check = CheckInflection('min',new_curr,new_pre,new_next);
                    
                    if(new_min_check && k ~= last_valley)
                        % If it has a lower amplitude than the current
                        % valley and there's atleast a sizable distance,
                        % update the values
                        if(new_curr < curr)
                            curr = new_curr;
                            new_index = k;
                            last_valley = k;
                            break;
                        end
                    end
                end
                valleys = vertcat(valleys,[new_index curr]);
                
                last_valley = new_index;
                break;
            end
        end
        % Search Foward to find peaks
        if(forward_end >=length(signal))
           forward_end = length(signal)-1;
        end
        
        for(j =curr_index:forward_end)
            pre = signal(j-1);
            curr =  signal(j);
            next = signal(j+1);
            max_check = CheckInflection('max',curr, pre, next);
            
            % Same goes for peaks
            if(max_check && j ~= last_peaks)
                last_peaks = j;
                peaks = vertcat(peaks, [j curr]);
                break;
            end
        end
    end
    
    derivative = z;
ti = toc;
end






