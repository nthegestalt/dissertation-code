%% Filename: ADT
%
% Description: Main code for the ADT Algorithm. Auxillary functions are
%              hosted in other files including:
%              -    /Helpers/CalculateThreshold.m
%              -    /Helpers/CheckInflection.m
%              -    /Helpers/CheckIntersect.m
%              -    /Helpers/InitThreshold.m
%              -    /Helpers/MovingStdDev.m
%
% Paper Reference:
%               -------------------------------------------------------------------------------------------------------------
%								Shin, H. S., Lee, C., & Lee, M. (2009). 
%								Adaptive threshold method for the peak detection of photoplethysmographic waveform. 
%								Comput. Biol. Med., 39(12), 1145-1152. doi: 10.1016/j.compbiomed.2009.10.006
%               -------------------------------------------------------------------------------------------------------------
% Author: Tuan
% Last Modified: 5th February 2016
%
% Param: signal, sampling rate, live mode?
% Return: max slope line, min slope line, peaks, valleys
%
function [max_slope,min_slope, peaks,valleys] = ADT(ppg_signal, sampling_freq, live_mode)
    % Initialise Algorithm Variables
    max_slope(1,1) = 0;
    max_slope(1,2) = 0;
    
    min_slope(1,1) = 0;
    min_slope(1,2) = 0;
    peaks(1,1) = 0;
    peaks(1,2) = 0;
    valleys(1,1) = 0;
    valleys(1,2) = 0;
    
    sample_size   = length(ppg_signal);     % Data length
    wait_period   = 10;                     % Samples to wait for before starting
    
    buffer_index = 1;
    buffer = 0;
    min_seeking_period = false;
    max_seeking_period = false;
    first_max_run = true;
    first_min_run = true;

    % Auxillary index buffers
    peak_index = 1;
    valley_index = 1;
    
    curr_mean = 0;
    curr_M2 = 0;
    next_valley = 0;
    max_peak = -inf;
    calib_flag = true;
    distance_discrim = false;
    valley_distance = 0;
    peak_distance = 0;
    
    % Loop through the available data and feed the buffer
    while buffer_index ~= sample_size-1
        
        % Perform calibration procedures once we have atleast 6 pairs of
        % peaks and valleys to obtain discrimination values from
        if(size(peaks,1)>=6 && size(valleys,1)>=6 && calib_flag == true)
            temp_peaks = [];
            temp_valleys = [];
            valley_pivot = 2;
            previous_valley = 0;
            
            i = 1;
            j = 2;
            
            for(i = 1:size(peaks,1))
                if(peaks(i,2) > 0.65*max_peak)
                  temp_peaks = vertcat(temp_peaks, peaks(i,:));
                  
                  % Find the valley that is the closest to the peak
                  % behind it
                  for(j = 2:size(valleys,1))
                      % The valley we want will always be the one behind
                      % the valley that surpasses the peak's x co-ordinates
                      if(valleys(j,1) > peaks(i,1))
                          c_index  = valleys(j-1,1)
                          c_valley = valleys(j-1,2);
                          
                          % Valley Verification
                          [c_index,c_valley] = SearchBack(ppg_signal, c_index);
                          temp_valleys = vertcat(temp_valleys, [c_index,c_valley]);
                          
                          % Start from the point where we last detected it
                          j = j - 1;
                          break;
                      end
                  end
                end
            end
            
            % Calculate the average distance of the valleys and peaks
            valley_distance = mean(abs(diff(temp_valleys(:,1))));
            peak_distance = mean(abs(diff(peaks(:,1))));
            
            % Search the end of the window for any valleys that may not
            % exist behind a peak.
            for(k = j-2:size(valleys,1))
               if(valleys(k,1) - temp_valleys(end,1) > valley_distance * 0.85)
                   temp_valleys = vertcat(temp_valleys, valleys(k,:));
               end
            end
            
            valleys = temp_valleys;
            valley_index = size(valleys,1)+1;
            peaks = temp_peaks;
            peak_index = size(peaks,1)+1;
            calib_flag = false;
            distance_discrim = true;
        end
        
        % Start the detection algorithm if we have more than x samples.
        % This condition is purely optional depending on the user.
        if (buffer_index > wait_period)
            % Calculate the stddev
            [stddev, curr_mean, curr_M2] = MovingStdDev(buffer_index, curr_mean, curr_M2, ppg_signal(buffer_index));
            
            %Max peak detection
            if(max_seeking_period == true)
                % We need to displace it in order to avoid next point
                % collision with the signal
                max_threshold = ppg_signal(buffer_index) + 0.2;

                % Set the slope line to follow the signal
                max_slope(buffer_index, 1) = buffer_index;
                max_slope(buffer_index,2) = max_threshold;

                % Check if this point is an inflection point
                isInflection = CheckInflection('max',ppg_signal(buffer_index), ...
                    ppg_signal(buffer_index-1), ...
                    ppg_signal(buffer_index+1));

                
                if(isInflection == true)

                    if(distance_discrim == true)
                        if(buffer_index-peaks(end,1) >= 0.60*peak_distance)
                            peaks(peak_index,1) = buffer_index;
                            peaks(peak_index,2) = ppg_signal(buffer_index);
                            peak_index = peak_index + 1;
                        end
                    else
                        peaks(peak_index,1) = buffer_index;
                        peaks(peak_index,2) = ppg_signal(buffer_index);

                        if(ppg_signal(buffer_index) > max_peak)
                            max_peak = ppg_signal(buffer_index);
                        end

                        peak_index = peak_index + 1;
                    end
                end
                
                if(isInflection == true)
                    max_seeking_period = false;
                end
            else
                % First run so we initialise the algorithm threshold
                % After first run re-iteratively re-calculate the threshold
                % line value for every new value added to the buffer
                if(first_max_run == true)
                    max_threshold = InitThreshold('max',ppg_signal(1:300));
                    first_max_run = false;
                else

                    current_amplitude = max_slope(buffer_index - 1,2);
                    previous_amplitude = 0;
                    if(length(peaks) > 1)
                        previous_amplitude = peaks(end,2);
                    else
                        previous_amplitude = 0;
                    end

                    max_threshold = CalculateThreshold('max',current_amplitude,previous_amplitude,...
                        stddev,sampling_freq);

                end
                max_slope(buffer_index, 1) = buffer_index;
                max_slope(buffer_index, 2) = max_threshold;

                past_max_x = buffer_index - 1;
                past_max_y = max_slope(buffer_index - 1,2);
                curr_max_x = buffer_index;
                curr_max_y = max_slope(buffer_index,2);

                max_pre = [past_max_x past_max_y];
                max_curr = [curr_max_x curr_max_y]

                past_PPGx = buffer_index - 1;
                past_PPGy = ppg_signal(buffer_index - 1);
                curr_PPGx = buffer_index;
                curr_PPGy = ppg_signal(buffer_index);

                ppg_past = [past_PPGx past_PPGy];
                ppg_curr = [curr_PPGx curr_PPGy];

                isIntersect = CheckIntersect(max_pre, max_curr,ppg_past,ppg_curr);
                if(isIntersect)
                    max_seeking_period = true;
                end
            end
      
            % Min valley detection
            if(min_seeking_period == true)
                % We need to offset the threshold value by a tiny amount in
                % order to avoid re-detecting intersection as we are using
                % previous existing points to calculate the lines for
                % intersection testing.
                min_threshold = ppg_signal(buffer_index) - 0.2;

                % Set the slope line to follow the signal
                min_slope(buffer_index,1) = buffer_index;
                min_slope(buffer_index,2) = min_threshold;

                % Check whether it's an inflection point or not
                isInflection = CheckInflection('min',ppg_signal(buffer_index), ...
                    ppg_signal(buffer_index-1), ...
                    ppg_signal(buffer_index+1));

                
                if(isInflection)

                    % Apply distance discrimination for the valleys. We only consider
                    % this as a legit valley if is within distance range based on it's
                    % distance to it's neighbor.
                    if(distance_discrim == true)
                        if(buffer_index-valleys(end,1) >= 0.85*valley_distance)
                            % Just to be extra-sure search backwards and forwards a
                            % small amount in order to see if this is the real valley
                            % or not. Due to the collision of the slope it may not be
                            % the actual valley but instead a false which then makes
                            % the slope continue searching from there.
                            temp_min = ppg_signal(buffer_index);
                            temp_index = buffer_index;
                            [temp_index,temp_min] = SearchBack(ppg_signal, buffer_index);

                            valleys(valley_index,1) = temp_index;
                            valleys(valley_index,2) = temp_min;
                            valley_index = valley_index + 1;
                        end
                    else
                        valleys(valley_index,1) = buffer_index;
                        valleys(valley_index,2) = ppg_signal(buffer_index);
                        valley_index = valley_index + 1;
                    end

                end
                if(isInflection == true)
                    min_seeking_period = false;
                end
            else
              min_threshold = 0;
                 if(first_min_run == true)
                     min_threshold = InitThreshold('min',ppg_signal(1:300));
                     first_min_run = false;
                 else
                    current_amplitude = min_slope(buffer_index - 1,2);
                    previous_amplitude = 0;
                    if(length(valleys) > 1)
                        previous_amplitude = valleys(end,2);
                    else
                        previous_amplitude = 0;
                    end

                    min_threshold = CalculateThreshold('min',current_amplitude,previous_amplitude,...
                        stddev,sampling_freq);
                 end

                min_slope(buffer_index, 1) = buffer_index;
                min_slope(buffer_index, 2) = min_threshold;

                past_min_x = buffer_index - 1;
                past_min_y = min_slope(buffer_index - 1,2);
                curr_min_x = buffer_index;
                curr_min_y = min_slope(buffer_index,2);

                min_pre = [past_min_x past_min_y];
                min_curr = [curr_min_x curr_min_y];

                past_PPGx = buffer_index - 1;
                past_PPGy = ppg_signal(buffer_index - 1);
                curr_PPGx = buffer_index;
                curr_PPGy = ppg_signal(buffer_index);

                ppg_past = [past_PPGx past_PPGy];
                ppg_curr = [curr_PPGx curr_PPGy];

                isIntersect = CheckIntersect(min_pre, min_curr,ppg_past,ppg_curr);                
                if(isIntersect)
                    min_seeking_period = true;
                end
            end
            
        end
        buffer(buffer_index) = ppg_signal(buffer_index);
        buffer_index = buffer_index + 1;
        
        % Live mode display for debugging purposes
        if(live_mode == true)
            hFg = figure(4);
            set(hFg,'renderer','opengl');
            clf;
            plot(buffer);
            hold on;
            plot(max_slope(:,1),max_slope(:,2), 'magenta');
            plot(peaks(:,1),peaks(:,2), 'ro', 'color', 'magenta');
            plot(min_slope(:,1),min_slope(:,2), 'r');
            plot(valleys(:,1),valleys(:,2), 'ro');
            drawnow;
        end
    end
                    
end

function [index,value] = SearchBack(ppg_signal, buffer_index)
    index = buffer_index;
    value = ppg_signal(buffer_index);
    
    for(i = buffer_index-30:buffer_index+30)
        if(i+1 > length(ppg_signal))
            break;
        end
        t_pre = ppg_signal(i - 1);
        t_curr = ppg_signal(i);
        t_next = ppg_signal(i +1);

        t_min_check = CheckInflection('min',t_curr,t_pre,t_next);
        if(t_min_check == true && t_curr < ppg_signal(buffer_index))
            index = i;
            value = t_curr;
        end
    end
end