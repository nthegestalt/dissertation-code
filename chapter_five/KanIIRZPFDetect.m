%% File: KanIIRZPFDetect
%
%  Description: The detection algorithm presented by Kan et al. 2012.
%               -------------------------------------------------------------------------------------------------------------
%               Kan, L., et al. (2012). A cascade filter for pulse wave baseline drift elimination. 
%               Image and Signal Processing (CISP), 2012 5th International Congress on.
%               -------------------------------------------------------------------------------------------------------------
%
%  Author: Tuan
%  Last modified: 18th February 2016
%

function [valleys,peaks,missed_peaks_distance, ...
          missed_peaks_height, missed_valleys_height,ti] = KanIIRZPFDetect(signal)
 tic;
   % Preliminary initialisation
    valleys = [];
    peaks = [];
    missed_peaks_distance = [];
    missed_peaks_height = [];
    missed_valleys_height = [];
    
    signal_length = length(signal);
    sampling_rate = 300;
    
     % Ratios - Edit these as necessary
    PTVLowerRatio = 0.5;
    PTVUpperRatio = 2.5;
    PTPLowerRatio = 0.75;
    PTPUpperRatio = 2;
    BKSLowerRatio = 1.5;
       
    % Containers
    peaks = [];
    valleys = [];
    PTV_slopes = [];
    
    % Conditions
    last_valley_detected = 0;  % Last detected peak
    max_peak_val = -inf;       % Maximum peak value
    
    
    % 1. FFT the signal and take the frequency component with the highest
    % power to calculate the distance discrimination ratio
    signal_spectrum = abs(fft(signal));
    
    % Discard the upper half of the samples since the fft gives a mirrored result
    signal_spectrum = signal_spectrum(1:signal_length/2);
    normalized_index = sampling_rate*(0:signal_length/2-1)/signal_length;
    [max_freq,index] = max(signal_spectrum);
    
    % 2. Scale the index for the search window according to sampling rate
    normalized_freq_index = normalized_index(index);
    L1 = round((normalized_freq_index * sampling_rate));
    L2 = round((0.5 * L1));
    
    % 3. Initialise search variables and search the chosen segment 
    % for maximum values. The segement is selected at the start of the
    % signal rather than from random.
    segment_size = round(3 * sampling_rate * normalized_freq_index);
    next_peak_lowerbound = L2;
    next_peak_upperbound = L1 + L2;
    
    temp_PTV_slopes = [];
    max_slope_val = -inf;
    
    % Get all peaks
    for(i = 2:segment_size)
        pre = signal(i-1);
        curr = signal(i);
        next = signal(i+1);
        
        max_check = CheckInflection('max',curr, pre, next);
        
        % We also need to find the max peak value for discrimination
        if(max_check)
            for(j = i:-1:1)
                if(j-1 <= 1 || j - 2 <= 1 || j <= i-(L2*BKSLowerRatio))
                    break;
                end
                
                mi_pre = signal(j);
                mi_curr =  signal(j-1);
                mi_next = signal(j-2);
                
                min_check = CheckInflection('min',mi_curr, mi_pre, mi_next);
                if(min_check)
                    peaks = vertcat(peaks, [i, curr]);
                    temp_valley = mi_curr;
                    temp_index = j-1;
                    
                    % Validation process
                    for(k = j:-1:j-100)
                        if(k-2 <= 0)
                            break
                        end
      
                        t_mi_pre = signal(k);
                        t_mi_curr = signal(k-1);
                        t_mi_next = signal(k-2);
                        
                        t_min_check = CheckInflection('min',t_mi_curr,t_mi_pre, t_mi_next);
                        
                        if(t_min_check)
                            if(temp_valley > t_mi_curr)
                                temp_valley = t_mi_curr;
                                temp_index = k-1;
                            end
                        end
                    end
                    valleys = vertcat(valleys,[temp_index temp_valley]);
                    slope = ((peaks(end,2)-temp_valley)/ (peaks(end,1) - (temp_index)));
                    
                    if(slope > max_slope_val)
                        max_slope_val = slope;
                    end
                    
                    temp_PTV_slopes = vertcat(temp_PTV_slopes, slope);
                    break
                end
            end
        end
    end
    
    % Go through and discriminate based on slopes
    temp_peaks = [];
    temp_valleys = [];
    for(i = 1:length(temp_PTV_slopes))
       if(temp_PTV_slopes(i) >=  max_slope_val*PTVLowerRatio)
           PTV_slopes(end+1) = temp_PTV_slopes(i);
           
           temp_peaks = [temp_peaks;[peaks(i,1),peaks(i,2)]];
           temp_valleys = [temp_valleys;[valleys(i,1),valleys(i,2)]];
       end
    end
    
    peaks = temp_peaks;
    valleys = temp_valleys;
    last_valley_detected = valleys(end,1);
    
    % 6. Calculate the PTV discriminator factor
    APTVS = mean(PTV_slopes);
    APTVSLower = APTVS*PTVLowerRatio;
    APTVSUpper = APTVS*PTVUpperRatio;
    BKSLimit = L2*BKSLowerRatio;
    
    starting_point = peaks(end,1);
    
    % 7. Search the rest of the signal for peaks and then search backwards for
    %    the valley based on the PTP and PTV parameters that was established
    %    earlier
    for(i = starting_point:signal_length-1)
        % Search for the max
        pre = signal(i-1);
        curr = signal(i);
        next = signal(i+1);
        
        max_check = CheckInflection('max',curr, pre, next);
        
        if(max_check)
              if((i >= (next_peak_lowerbound*PTPLowerRatio)) ...
                   && (i <= (next_peak_upperbound*PTPUpperRatio)))
                   
                   next_peak_lowerbound = i + L1;
                   next_peak_upperbound = i + L1 + L2;
                   
                   % Now search backwards for the min from the peak
                   for(j = i:-1:1)
                       if(j-1 <= 0 || j - 2 <= 0 || j ...
                              <= i-(BKSLimit))
                           break;
                       end
                       
                       mi_pre = signal(j);
                       mi_curr =  signal(j-1);
                       mi_next = signal(j-2);
                       
                       % Check if the min exists and if the peak to valley
                       % distance is within range otherwise construct a min at
                       % the location using the peak as a reference point
                       min_check = CheckInflection('min',mi_curr, mi_pre, mi_next);
                       if(min_check)
                           slope = ((curr-mi_curr)/ (i - (j-1)));
                           
                           % APTV Discrimination
                           if((slope >= (APTVSLower)) ...
                                   && (slope <= APTVSUpper))
                               
                               % Only add if this is not the previous valley detected
                               if((j-1) ~= last_valley_detected)
                                   peaks = vertcat(peaks,[i curr]);

                                   temp_valley = mi_curr;
                                   temp_index = j-1;
                                    
                                   % Validation process
                                   for(k = j:-1:j-100)
                                       if(k-2 <= 0)
                                           break
                                       end
                                       
                                       t_mi_pre = signal(k);
                                       t_mi_curr = signal(k-1);
                                       t_mi_next = signal(k-2);

                                       t_min_check = CheckInflection('min',t_mi_curr,t_mi_pre, t_mi_next);

                                       if(t_min_check)
                                           if(temp_valley > t_mi_curr)
                                               temp_valley = t_mi_curr;
                                               temp_index = k-1;
                                           end
                                       end
                                   end

                                   valleys = vertcat(valleys,[temp_index temp_valley]);
                                   last_valley_detected = j-1;
                               end
                               break;
                           else
                               missed_peaks_height = vertcat(missed_peaks_height,[i curr]);
                               missed_valleys_height = vertcat(missed_valleys_height,[j mi_curr]);                     
                           end
                       end
                   end
              else
                   % If we've missed a feature, there might be an error
                   % spot so increment the search windows to look further
                   % ahead
                   next_peak_lowerbound = i + L2;
                   next_peak_upperbound = i + L1 + L2;
                   missed_peaks_distance = vertcat(missed_peaks_distance,[i curr]);
              end
        end 
    end
ti = toc;
end

