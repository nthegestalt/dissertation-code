%% Filename: DisplayDetResultsGeneric
%
% Description: Displays generic detection results with only peaks, valleys
%              and the filtered signal
% Author: Tuan
% Last Modified: 5th February 2016
% 
% Params: max slope line,min slope line,peaks,valleys
%  

function DisplayDetResultsGeneric( signal, peaks,valleys,method )
    figure;
    clf;
    set(gcf, 'Position', [0 0 8*100, 4*100]); %<- Set size
    set(gca, 'FontSize', 13, 'LineWidth', 0.75,'FontName','CMU Sans Serif Demi Condensed','FontWeight','bold'); %<- Set properties
    hold on;
    box on;
    
    title(['Detection Results for ' method]);
    plot(signal,'k','LineWidth',1.1);
    plot(peaks(:,1),peaks(:,2),'ro','LineWidth',1.1,'MarkerFaceColor','r','MarkerSize',7);
    plot(valleys(:,1),valleys(:,2),'bo','LineWidth',1.1,'MarkerFaceColor','b','MarkerSize',7);
end

