%% File: DisplayWaveletFeatureDetection
%
%  Description: Display function for the WaveletCascadedDetect file.
%
%  Author: Tuan
%  Last modified: 18th December 2015
%
%  Param: signal, derivative peaks, derivative peak indices, valleys
%         valley indices, peaks, peaks indices, derivative
function DisplayWaveletFeatureDetection (signal, deriv_peaks,deriv_peaks_indices, ...
                                  valleys, valleys_indices, peaks, peaks_indices, deriv)
    %Chart Parameters
    width = 8;     % Width in inches
    height = 5.3;    % Height in inches
    alw = 0.75;    % AxesLineWidth
    fsz = 14;      % Fontsize
    lw = 1.0;      % LineWidth
    msz = 10;       % MarkerSize
    fnt = 'Times';
    
    figure(2);
    clf;
    set(gcf,'renderer','opengl');
    set(gcf, 'Position', [0 0 width*100, height*100]); %<- Set size
    
    subplot(2,1,1);
    set(gca, 'FontSize', fsz, 'LineWidth', alw,'FontName',fnt, 'FontWeight','bold'); %<- Set properties
    box on;
    hold on;
    plot(deriv,'LineWidth',lw,'Color','k');
    plot(deriv_peaks_indices,deriv_peaks,'o','MarkerSize',5,'LineWidth',lw,'Color',[1,0,0]);
    title('Derivative Peaks');
    
    subplot(2,1,2);
    set(gcf, 'Position', [0 0 width*100, height*100]); %<- Set size
    set(gca, 'FontSize', fsz, 'LineWidth', alw,'FontName',fnt, 'FontWeight','bold'); %<- Set properties
    box on;
    hold on;
    plot(signal,'LineWidth',lw,'Color','k');
    deriv_p_line = plot(deriv_peaks_indices,deriv_peaks,'o','MarkerSize',5,'LineWidth',lw,'Color',[1,0,0]);
    valley_line = scatter(valleys_indices,valleys,75,'x','MarkerFaceColor',[0.0 0 0.5],'MarkerEdgeColor',[0.0 0 1],'LineWidth',1.5);
    peak_line = scatter(peaks_indices,peaks,75,'x','MarkerFaceColor',[1.0 0 0.0],'MarkerEdgeColor',[1.0 0 0],'LineWidth',1.5);
    title('Features detected from Derivative Peaks');

    legend([deriv_p_line,valley_line,peak_line],{'Derivative Peaks','Valleys','Peaks'},'Location',[0.2, 0.005, 0.6, 0.05],'Orientation','Horizontal');
    legend('boxoff');
    
end