%% Filename: DisplayADT
%
% Description: Displays results of the ADT algorithm
% Author: Tuan
% Last Modified: 5th February 2016
% 
% Params: max slope line,min slope line,peaks,valleys
%  

function DisplayADT(conditioned_signal,max_slope,min_slope,peaks,valleys)  
    hFig = figure();
    set(gcf,'renderer','opengl');
    set(hFig, 'Position', [0 0 8*100 5.3*100])
    clf;
    subplot(2,1,1);
    set(gca, 'FontSize', 14, 'LineWidth', 0.75,'FontName','CMU Sans Serif Demi Condensed'); %<- Set properties
    hold on;
    box on;
    plot(max_slope(:,1),max_slope(:,2), 'k','LineWidth',1.1);
    plot(peaks(:,1),peaks(:,2),'o','LineWidth',1.1, 'MarkerSize',5,'MarkerFaceColor','r','MarkerEdgeColor','r');
    plot(conditioned_signal,'b','LineWidth',1.1);
    title('Detected Peaks');
    
    subplot(2,1,2);
    set(gca, 'FontSize', 14, 'LineWidth', 0.75,'FontName','CMU Sans Serif Demi Condensed'); %<- Set properties
    hold on;
    box on;
    plot(conditioned_signal,'b','LineWidth',1.1);
    plot(valleys(:,1),valleys(:,2),'o','LineWidth',1.1, 'MarkerSize',5,'MarkerFaceColor','r','MarkerEdgeColor','r');
    plot(min_slope(:,1),min_slope(:,2), 'k','LineWidth',1.1);
    title('Detected Valleys');
    set(gcf,'PaperUnits','inches','PaperPositionMode','auto');
end