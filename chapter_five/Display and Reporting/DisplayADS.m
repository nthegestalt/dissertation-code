%% Filename: DisplayADS
%
% Description: Displays results of the ADS algorithm
% Author: Tuan
% Last Modified: 5th February 2016
% 
% Params: signal, peaks, valleys, line segments
%  
function DisplayADS(fir_csg,peaks,valleys,line_segments)
    figure();
    clf;
    set(gcf,'renderer','opengl');
    subplot('position', [0.1, 0.2, 0.81, 0.7]);
    set(gcf, 'Position', [0 0 8*100, 4*100]); %<- Set size
    set(gca, 'FontSize', 14, 'LineWidth', 1.0,'FontName','CMU Sans Serif Demi Condensed'); %<- Set properties
    hold on;
    box on;
    plot(fir_csg,'Color','k','LineWidth',1.1);
    
    full_valleys = [];
    peaks = [];
    valleys = [];
    
    for(i = 1:size(line_segments))
        p1 = [line_segments(i,1);line_segments(i,3)]
        valleys = [valleys;[line_segments(i,3),line_segments(i,4)]];
        full_valleys = [full_valleys;[line_segments(i,1);line_segments(i,2)]'];
        p2 = [line_segments(i,2) line_segments(i,4)]
        peaks = [peaks;[line_segments(i,1),line_segments(i,2)]];
        
        valley = plot(line_segments(i,1),line_segments(i,2),'x','LineWidth',1.5, 'MarkerSize',10,'MarkerFaceColor','r','MarkerEdgeColor','r');
        peak = plot(line_segments(i,3),line_segments(i,4),'x','LineWidth',1.5, 'MarkerSize',10,'MarkerFaceColor','b','MarkerEdgeColor','b');
%         slope = plot(p1',p2','Color',[1 0 0],'LineWidth',1.2,'LineSmoothing','on');
        slope = plot(p1',p2','Color',[1 0 0],'LineWidth',1.2);
    end
    title('Slopes, Peaks and Valleys from ADS Algorithm');
    legend([slope,valley,peak],{'Slope','Valley','Peak'},'Location',[0.2, 0.05, 0.6, 0.05],'Orientation','horizontal');
    legend('boxoff');
end