%% File: DisplayWaveletCascadeDet
%
%  Description: A Helper function for displaying and debugging the Wavelet
%               Cascaded algorithm's results.
%
%  Parameters: derivative, derivative peaks, wavelet filtered signal,
%              peaks, valleys,file name, save flag
%
%  Author: Tuan
%  Last modified: 19th December 2015k
%

function DisplayWaveletCascadeDet( derivative, derivative_peaks, filtered_signal, ...
                                   peaks, valleys, file_name, save_flag )
    %Chart Parameters
    width = 8;     % Width in inches
    height = 5.3;  % Height in inches
    alw = 0.75;    % AxesLineWidth
    fsz = 14;      % Fontsize
    lw = 1.0;      % LineWidth
    msz = 10;      % MarkerSize
    fnt = 'CMU Sans Serif Demi Condensed';
    
    figure;
    clf;
    set(gcf,'renderer','opengl');
    set(gcf, 'Position', [0 0 width*100, height*100]); %<- Set size
    
    subplot(2,1,1);
    set(gca, 'FontSize', fsz, 'LineWidth', alw,'FontName',fnt, 'FontWeight','bold'); %<- Set properties
    box on;
    hold on;
    plot(derivative,'LineWidth',lw,'Color','k');
    plot(derivative_peaks(:,1),derivative_peaks(:,2),'o','MarkerSize',5,'LineWidth',lw,'Color',[1,0,0]);
    title('Derivative Peaks');
    
    subplot(2,1,2);
    set(gcf, 'Position', [0 0 width*100, height*100]); %<- Set size
    set(gca, 'FontSize', fsz, 'LineWidth', alw,'FontName',fnt, 'FontWeight','bold'); %<- Set properties
    box on;
    hold on;
    plot(filtered_signal,'LineWidth',lw,'Color','k');
    deriv_p_line = plot(derivative_peaks(:,1),derivative_peaks(:,2),'o','MarkerSize',5,'LineWidth',lw,'Color',[1,0,0]);
    valley_line = scatter(valleys(:,1),valleys(:,2),75,'x','MarkerFaceColor',[0.0 0 0.5],'MarkerEdgeColor',[0.0 0 1],'LineWidth',1.5);
    peak_line = scatter(peaks(:,1),peaks(:,2),75,'x','MarkerFaceColor',[1.0 0 0.0],'MarkerEdgeColor',[1.0 0 0],'LineWidth',1.5,'Visible','off');
    title('Features detected from Derivative Peaks');

    legend([deriv_p_line,valley_line,peak_line],{'Derivative Peaks','Valleys','Peaks'},'Location',[0.2, 0.005, 0.6, 0.05],'Orientation','Horizontal');
    legend('boxoff');
    
    if(save_flag == true)
        fig_label = [file_name '_Lisheng_Detection'];
        set(gcf,'PaperUnits','inches','PaperPositionMode','auto')
        
        print(gcf,fig_label,'-depsc2','-loose');
        system(['convert -density 300 ',fig_label,'.eps ',fig_label,'.png'])
        savefig(fig_label);
    end
end

