%% Filename: DisplayKanIIRZPF
%
% Description: Displays results of Kan et al. 2012's algorithm
% Author: Tuan
% Last Modified: 5th February 2016
% 
% Params: signal, peaks, valleys, line segments
%  
function DisplayKanIIRZPF(csg,valleys_a,peaks_a,missed_peaks_distance,...
                          missed_peaks_height,missed_valleys_height,file_name, save_flag)
    %Chart Parameters
    width = 8;     % Width in inches
    height = 5.3;    % Height in inches
    alw = 0.75;    % AxesLineWidth
    fsz = 14;      % Fontsize
    lw = 1.1;      % LineWidth
    msz = 8;       % MarkerSize
    fnt = 'CMU Sans Serif Demi Condensed';
    
    hFig = figure(304);
    set(gcf,'renderer','opengl');
    set(hFig, 'Position', [0 0 width*100 height*100])
    clf;
    subplot(2,1,1);
    box on;
    hold on;
    set(gca, 'FontSize', fsz, 'LineWidth', alw,'FontName',fnt,'FontWeight','bold'); %<- Set properties
    plot(csg,'LineWidth',lw,'Color','k');
    title('Detected Features');
    h1_n = plot(peaks_a(:,1),peaks_a(:,2),'ro', 'LineWidth',lw,'MarkerSize',7,'MarkerFaceColor','r');
    h2_n = plot(valleys_a(:,1),valleys_a(:,2),'bo', 'LineWidth',lw,'MarkerSize',7,'MarkerFaceColor','b');

    subplot(2,1,2);
    box on;
    hold on;
    set(gca, 'FontSize', fsz, 'LineWidth', alw,'FontName',fnt,'FontWeight','bold'); %<- Set properties
    plot(csg,'LineWidth',lw,'Color','k');
    title('Excluded Features');
    set(hFig,'PaperUnits','centimeters','PaperPositionMode','auto');
    
    plot(csg,'LineWidth',lw,'Color','k');
    if(~isempty(missed_peaks_distance))
        h1_e = plot(missed_peaks_distance(:,1),missed_peaks_distance(:,2),'o','MarkerEdgeColor',[0.8510    0.3255    0.0980], 'LineWidth',lw,'MarkerSize',7,'MarkerFaceColor',[0.8510    0.3255    0.0980]);
    else
        h1_e = plot(0,0,'go', 'LineWidth',lw,'MarkerSize',5,'MarkerFaceColor','g','Visible','off');
    end
    if(~isempty(missed_peaks_height))
        h2_e = plot(missed_peaks_height(:,1),missed_peaks_height(:,2),'o','MarkerEdgeColor',[0.6353    0.0784    0.1843], 'LineWidth',lw,'MarkerSize',7,'MarkerFaceColor',[0.6353    0.0784    0.1843]);
    else
        h2_e = plot(0,0,'go', 'LineWidth',lw,'MarkerSize',5,'MarkerFaceColor','g','Visible','off');
    end
    if(~isempty(missed_valleys_height))
        h3_e = plot(missed_valleys_height(:,1),missed_valleys_height(:,2),'o','MarkerEdgeColor',[0.1647    0.3843    0.2745], 'LineWidth',lw,'MarkerSize',7,'MarkerFaceColor',[0.1647    0.3843    0.2745]);
    else
        h3_e = plot(0,0,'go', 'LineWidth',lw,'MarkerSize',5,'MarkerFaceColor','g','Visible','off');
    end
    
    if(save_flag == true)
        fig_label = [file_name '_Kan_FIR_Segmentation'];
        set(gcf,'PaperUnits','inches','PaperPositionMode','auto')
        print(gcf,fig_label,'-depsc2','-loose');
        savefig(fig_label);
        system(['convert -density 300 ',fig_label,'.eps ',fig_label,'.png'])
    end
end