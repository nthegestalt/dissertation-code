%% File: GenerateDetectionReport
%
%  Description: Generates a report of detected features for methods
%
%  Author: Tuan
%  Last modified: 5th December 2015
%
%  Param: peaks, valleys, run time, file name, method  
%
function GenerateDetectionReport(peaks,valleys,ti,file_name,method)
    disp('****************************************');
    disp([method ' DETECTION REPORT FOR: ' file_name]);
    disp('****************************************');
    disp(['Total Time: ' sprintf('%f',ti)]);
    disp(['Total Peaks: ' sprintf('%f',length(peaks))]);
    disp(['Total Valleys: ' sprintf('%f',length(valleys))]);
end
